const request = require("request-promise-native");
const { Router } = require("express");
const dotenv = require("dotenv");
const router = Router();

dotenv.config();
const API_SERVER = process.env.AADHAAR_API_SERVER;

router.post("/verify/gst", (req, res) => {
  request
    .post({
      url: `${API_SERVER}/verify-gst`,
      headers: {
        qt_api_key: process.env.AGENCY_API_KEY,
        qt_agency_id: process.env.AGENCY_ID
      },
      body: {
        gstin: req.body.gstin,
        consent: req.body.consent ? "Y" : "N",
        consent_text: req.body.consent_text
      },
      json: true
    })
    .then(response => res.status(200).json(response))
    .catch(error => {
      console.error(error);
      res.status(400).json({
        error: true
      });
    });
});

module.exports = router;
