const {Router} = require('express')
const router =  Router()

router.post('/verify/fssai', (req, res) => {
    res.status(200).json({
        fssai: true
    })
})

module.exports = router