const express = require("express");
const dotenv = require("dotenv");
const bodyParser = require("body-parser");
const controller = require("./controller");

dotenv.config();

const app = express();

app.use(bodyParser.json({ extended: true }));
app.use("/api/", controller);

app.listen(process.env.PORT, () => {
  console.log(`Start server: http://localhost:${process.env.PORT}`);
});
