const {Router} = require('express')

const router = Router()

const gstRouter = require('./gst-verification/gst-verification-controller')
const FSSAIRouter = require('./fssai-verification/fssai-verification-controller')

router.use(gstRouter)
router.use(FSSAIRouter)

module.exports = router
