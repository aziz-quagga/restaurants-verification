import React from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import axios from "axios";
import "./App.css";
import { FormControlLabel, Checkbox } from "@material-ui/core";

const consentText = "I give my consent to aadhaar api to fetch my details";

function App() {
  const [gstin, setGSTIN] = React.useState("");
  const [consent, setConsent] = React.useState(false);
  const [value, setValue] = React.useState("");
  const [isSubmitting, setIsSubmitting] = React.useState(false);
  function handleGSTFetch(gstin, consent) {
    setIsSubmitting(true);
    axios
      .post("/api/verify/gst", {
        gstin,
        consent,
        consent_text: consentText
      })
      .then(response => {
        setValue(JSON.stringify(response.data, null, 4));
      })
      .finally(() => {
        setIsSubmitting(false);
      });
  }
  return (
    <Grid container className="App">
      <Grid item sm={12}>
        <TextField
          label="GSTIN"
          value={gstin}
          onChange={event =>
            setGSTIN(
              event.target.value && String(event.target.value).toUpperCase()
            )
          }
          InputLabelProps={{
            shrink: true
          }}
          margin="normal"
          variant="outlined"
        />
      </Grid>
      <Grid item sm={12}>
        <FormControlLabel
          control={
            <Checkbox
              checked={consent}
              onChange={event => setConsent(event.target.checked)}
              value="checkedB"
              color="primary"
            />
          }
          label={consentText}
        />
      </Grid>
      <Grid item sm={12}>
        <Button
          variant="contained"
          color="primary"
          disabled={isSubmitting}
          onClick={() => handleGSTFetch(gstin, consent)}
        >
          Get
        </Button>
      </Grid>
      <Grid item sm={12}>
        {JSON.stringify(value)}
      </Grid>
    </Grid>
  );
}

export default App;
